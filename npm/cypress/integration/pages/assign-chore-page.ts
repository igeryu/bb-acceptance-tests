import { expect } from "chai";

import {
    CHORES_URL,
    STUBBING,
} from "../common/test-config";

const CHILDS_CHORES_URL_PREFIX = CHORES_URL + '/ui/child/';
const ASSIGN_CHORE_URL_SUFFIX = '/chores/assign';

export default class AssignChorePage {

    static view_Assign_Chore_page(childId: number, childFirstName: string): void {
        cy.wrap(childFirstName).as('childFirstName').then((): void => {
            const child = { firstName: childFirstName };
            cy.intercept('GET', `api/children/${childId}`, child);
            cy.visit(CHILDS_CHORES_URL_PREFIX + childId + ASSIGN_CHORE_URL_SUFFIX);
        });
    }

    static is_on_page(isDisplayed: boolean): void {
        cy.get('@childFirstName').then((childFirstName: string): void => {
            if (isDisplayed) {
                const childName = childFirstName;
                cy.get('h1')
                    .should('contain', 'Assign Chore to ' + childName);
            } else {
                cy.get('h1')
                    .should('not.contain', 'Assign Chore to');
            }
        });
    }

    static Add_Chore_button(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=add-chore]');
    }

    static add_Chore_button_is_visible(): void {
        this.Add_Chore_button()
            .should('be.visible');
    }

    static click_Add_Chore_button(): void {
        this.Add_Chore_button().click();
    }

    static Chore_buttons(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=available-chore]');
    }

    static Chore_button_list(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=available-chore-list]');
    }

    static Chore_button(choreName: string): Chainable<JQuery<HTMLElement>> {
        return this.Chore_buttons()
            .contains(choreName);
    }

    static Chore_is_displayed(choreName: string): void {
        this.Chore_button(choreName)
            .should('be.visible');
    }

    static click_Chore_button(choreName: string): void {
        this.Chore_button(choreName).click();
    }

    static Chore_is_not_displayed(choreName: string): void {
        this.Chore_button_list()
            .should('not.contain', choreName);
    }

    static confirmation_is_displayed(): void {
        cy.get('[data-cy=confirmation]')
            .should('be.visible');
    }

    static confirmation_should_say(expectedMessage: string): void {
        cy.get('[data-cy=confirmation]')
            .should('contain', expectedMessage);
    }

    static confirmation_should_have_option(expectedOption: string): void {
        cy.get('[data-cy=confirmation-option]')
            .should('contain', expectedOption);
    }

    static confirmation_select_option(selectedOption: string): void {
        if (STUBBING) {
            cy.get('@childId').then((childId: number): void => {
                if (selectedOption === 'Yes') {
                    cy.get('@choreName').then((choreName: string): void => {
                        const assignChoreResponse = this.emptyResponseWithStatusCode(201);
                        cy.get('@choreId').then((choreId: number): void => {
                            cy.get('@choresAssignedResponse').then((choresAssignedResponse): void => {

                                cy.intercept('PUT', `api/children/${childId}/chores`, (request): void => {
                                    expect(request.body.id).to.equal(choreId);
                                    choresAssignedResponse.push({ name: choreName });
                                    request.reply(assignChoreResponse);
                                }).as('assign-chore');
                            });
                        });
                    });
                }
            });
        } else {
            // Not stubbing
            cy.intercept('GET', 'api/children/*/chores').as('get-assigned-chores');
            if (selectedOption === 'Yes') {
                cy.intercept('PUT', 'api/children/*/chores').as('assign-chore');
            }
        }

        cy.get('[data-cy=confirmation-option]')
            .contains(selectedOption)
            .click();


        if (selectedOption === 'Yes') {
            cy.wait('@assign-chore');
        }
        cy.wait('@get-assigned-chores');
    }

    static emptyResponseWithStatusCode(statusCode: number): StaticResponse {
        return {
            statusCode: statusCode,
            body: {},
        }
    }

    static confirmation_is_not_displayed(): void {
        cy.get('[data-cy=confirmation]')
            .should('not.exist');
    }

}