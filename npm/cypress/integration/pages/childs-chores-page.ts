import { CHORES_URL } from '../common/test-config';

const CHILDS_CHORES_URL_PREFIX = CHORES_URL + '/ui/child/';
const CHILDS_CHORES_URL_SUFFIX = '/chores';

export default class ChildsChoresPage {

    static viewChildsChoresPage(childId: number, childFirstName: name): void {
        cy.wrap(childFirstName).as('childFirstName').then((): void => {
            const child = { firstName: childFirstName };
            cy.intercept('GET', `api/children/${childId}`, child);
            cy.visit(CHILDS_CHORES_URL_PREFIX + childId + CHILDS_CHORES_URL_SUFFIX);
        });
    }

    static is_on_page(isDisplayed: boolean): void {
        cy.get('@childFirstName').then((childFirstName: string): void => {
            if (isDisplayed) {
                cy.get('h1')
                    .should('contain', childFirstName + '\'s Assigned Chores');
            } else {
                cy.get('h1')
                    .should('not.contain', 'Assigned Chores');
            }
        });
    }

    static add_Chore_to_stubbed_response(): void {
        cy.get('@choreName').then((choreName: string): void => {
            cy.get('@choreId').then((choreId: number): void => {
                cy.get('@choresAssignedResponse').then((choresAssignedResponse): void => {
                    choresAssignedResponse.push({ id: choreId, name: choreName });
                });
            });
        });
    }

    static assignChoreButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=assign-chore]');
    }

    static Assign_Chore_button_is_visible(): void {
        ChildsChoresPage.assignChoreButton()
            .should('be.visible');
    }

    static click_Assign_Chore_button(): void {
        ChildsChoresPage.assignChoreButton().click();
    }

    static Chore_button(choreName: string): Chainable<JQuery<HTMLElement>> {
        return this.assigned_Chores()
            .contains(choreName);
    }

    static Chore_is_Assigned(choreName: string): void {
        this.Chore_button(choreName)
            .should('be.visible');
    }

    static Chore_is_not_Assigned(choreName: string): void {
        this.assigned_Chores()
            .should('not.contain', choreName);
    }

    static no_Chores_are_Assigned(): void {
        this.assigned_Chores()
            .children()
            .should('have.length', 0);
    }

    static assigned_Chores(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=assigned-chore-list]');
    }
}