import {
    STUBBING,
    SIGNIN_URL
} from '../common/test-config';

export default class SignInPage {

    static viewSignInPage(): void {
        cy.visit(SIGNIN_URL);
    }

    static is_on_page(isDisplayed: boolean): void {
        if (isDisplayed) {
            cy.get('h1')
                .should('contain', 'Sign In');
        } else {
            cy.get('h1')
                .should('not.contain', 'Sign In');
        }
    }

    static emailInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=email]');
    }

    static emailInputIsVisible(): void {
        SignInPage.emailInput().should('be.visible');
    }

    static enter_email(email: string): void {
        SignInPage.emailInput().type(email);
    }

    static passwordInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=password]');
    }

    static passwordInputIsVisible(): void {
        SignInPage.passwordInput().should('be.visible');
    }

    static enterPasswords(password: string): void {
        SignInPage.passwordInput().type(password);
    }

    static submitButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=submit]');
    }

    static Submit_button_is_visible(): void {
        SignInPage.submitButton()
            .should('be.visible')
            .should('have.attr', 'type', 'submit');
    }

    static click_Submit(): void {
        if (STUBBING) {
            const fakeToken = 'fake_encoded_header.' + btoa(JSON.stringify({ authorities: ['ROLE_PARENT'] })) + '.fake_encoded_signature';
            const response = {
                access_token: fakeToken,
            };
            cy.intercept('POST', 'api/auth/oauth/token', response).as('sign-in');
        } else {
            cy.intercept('POST', 'api/auth/oauth/token').as('sign-in');
        }

        SignInPage.submitButton().click();

        cy.wait('@sign-in').then(({response}): void => {
            expect(response.statusCode).to.eq(200);
        });
    }

    static errorMessagesAreNotVisible(): void {
        cy.get('.error')
            .should('not.exist');
    }
}