import { DASHBOARD_URL } from '../common/test-config';

export default class DashboardPage {

    static viewDashboardPage(): void {
        cy.visit(DASHBOARD_URL);
    }

    static is_on_page(isDisplayed: boolean): void {
        if (isDisplayed) {
            cy.get('h1')
                .should('contain', 'Children');
        } else {
            cy.get('h1')
                .should('not.contain', 'Children');
        }
    }

    static addChildButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=add-child]');
    }

    static addChildButtonIsVisible(): void {
        DashboardPage.addChildButton()
            .should('be.visible');
    }

    static click_Add_Child_button(): void {
        DashboardPage.addChildButton().click()
    }

    static childButton(firstName: string): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=child-' + firstName + ']');
    }

    static childIsDisplayed(firstName: string): void {
        DashboardPage.childButton(firstName)
            .should('be.visible');
    }

    static click_Child_button(firstName: string): void {
        DashboardPage.childIsDisplayed(firstName);
        DashboardPage.childButton(firstName).click();
    }

    static childIsNotDisplayed(firstName: string): void {
        DashboardPage.childButton(firstName)
            .should('not.exist');
    }
}