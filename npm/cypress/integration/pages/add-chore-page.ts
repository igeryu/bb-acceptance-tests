import {
    CHORES_URL,
    STUBBING,
} from "../common/test-config";

const CHILDS_CHORES_URL_PREFIX = CHORES_URL + '/ui/child/';
const ADD_CHORE_URL_SUFFIX = '/chores/add';

export default class AddChorePage {

    static view_Add_Chore_page(childId: number, childFirstName: string): void {
        cy.wrap(childFirstName).as('childFirstName').then((): void => {
            const child = { firstName: childFirstName };
            cy.intercept('GET', `api/children/${childId}`, child);
            cy.visit(CHILDS_CHORES_URL_PREFIX + childId + ADD_CHORE_URL_SUFFIX);
        });
    }

    static is_on_page(isDisplayed: boolean): void {
        if (isDisplayed) {
            cy.get('h1')
                .should('contain', 'Add Chore');
        } else {
            cy.get('h1')
                .should('not.contain', 'Add Chore');
        }
    }

    static nameInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=chore-name]');
    }

    static name_input_is_visible(): void {
        AddChorePage.nameInput()
            .should('be.visible')
            .should('have.value', '');
    }

    static enter_name(taskName: string): void {
        AddChorePage.nameInput().type(taskName);
    }

    static submitButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=submit]');
    }

    static Submit_button_is_visible(): void {
        AddChorePage.submitButton()
            .should('be.visible');
    }

    static createChoreResponse(statusCode: string): StaticResponse {
        return {
            statusCode: statusCode,
            body: {},
        }
    }

    static click_Submit(expectingRequest: boolean, expectingSuccess: boolean): void {
        if (STUBBING) {
            const statusCode = expectingSuccess ? 201 : 500;
            if (expectingSuccess) {
                this.add_Chore_to_stubbed_response();
            }
            const response = this.createChoreResponse(statusCode);
            cy.intercept('POST', 'api/chores', response).as('add-chore');
        } else {
            cy.intercept('POST', 'api/chores').as('add-chore');
        }

        AddChorePage.submitButton().click();

        if (expectingRequest) {
            cy.wait('@add-chore');
        }
    }

    static add_Chore_to_stubbed_response(): void {
        cy.get('@choreName').then((choreName: string): void => {
            cy.get('@choresAvailableResponse').then((choresAvailableResponse): void => {
                const choreId = Math.trunc(Math.random() * 1000);
                choresAvailableResponse.push({ id: choreId, name: choreName });
                cy.wrap(choreId).as('choreId');
            });
        });
    }

    static click_Submit_duplicate(): void {
        if (STUBBING) {
            const response = this.createChoreResponse(409);
            cy.intercept('POST', 'api/chores', response);
        } else {
            throw new Error('Should not be calling click_Submit_duplicate if not stubbing');
        }

        AddChorePage.submitButton().click();
    }

    static cancelButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=cancel]');
    }

    static Cancel_button_is_visible(): void {
        AddChorePage.cancelButton()
            .should('be.visible');
    }

    static click_Cancel_button(): void {
        AddChorePage.cancelButton().click();
    }

    static error(): Chainable<JQuery<HTMLElement>> {
        return cy.get('.error');
    }

    static error_messages_are_visible(): void {
        AddChorePage.error().should('be.visible');
    }

    static error_message_contains(message: string): void {
        AddChorePage.error().should('contain', message);
    }

}