import { STUBBING } from '../common/test-config';

export default class AddChildPage {
    static is_on_page(): void {
        cy.get('h1')
            .should('contain', 'Add Child');
    }

    static firstNameInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=first-name]');
    }

    static firstNameInputIsVisible(): void {
        AddChildPage.firstNameInput()
            .should('be.visible')
            .should('have.value', '');
    }

    static enter_first_name(firstName: name): void {
        AddChildPage.firstNameInput().type(firstName);
    }

    static emailInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=email]');
    }

    static emailInputIsVisible(): void {
        AddChildPage.emailInput()
            .should('be.visible')
            .should('be.empty');
    }

    static enter_email(email: string): void {
        AddChildPage.emailInput().type(email);
    }

    static submitButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=submit]');
    }

    static cancelButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=cancel]');
    }

    static Submit_button_is_visible(): void {
        AddChildPage.submitButton()
            .should('be.visible')
            .should('have.attr', 'type', 'submit');
    }

    static Cancel_button_is_visible(): void {
        AddChildPage.cancelButton()
            .should('be.visible')
            .should('have.attr', 'type', 'cancel');
    }

    static createChildResponse(statusCode: string): StaticResponse {
        return {
            statusCode: statusCode,
            body: {},
        }
    }

    static click_Submit(expectingRequest: boolean, expectingSuccess: boolean): void {
        if (STUBBING) {
            const statusCode = expectingSuccess ? 201 : 500;
            const response = this.createChildResponse(statusCode);
            cy.get('@allChildrenResponse').then((allChildrenResponse: StaticResponse[]): void => {
                cy.intercept('POST', 'api/children', (request: StaticResponse): void => {
                    allChildrenResponse.push({...request.body, id: 1});
                    request.reply(response);
                }).as('add-child');
            });
        } else {
            cy.intercept('POST', 'api/children').as('add-child');
        }

        AddChildPage.submitButton().click();

        if (expectingRequest) {
            cy.wait('@add-child');
        }
    }

    static click_Submit_duplicate(): void {
        if (STUBBING) {
            const response = this.createChildResponse(409);
            cy.intercept('POST', 'api/children', response);
        } else {
            throw new Error('Should not be calling click_Submit_duplicate if not stubbing');
        }

        AddChildPage.submitButton().click();
    }

    static clickCancel(): void {
        AddChildPage.cancelButton().click();
    }

    static error(): Chainable<JQuery<HTMLElement>> {
        return cy.get('.error');
    }

    static errorMessagesAreNotVisible(): void {
        AddChildPage.error().should('not.exist');
    }

    static error_messages_are_visible(): void {
        AddChildPage.error().should('be.visible');
    }

    static error_message_contains(message: string): void {
        AddChildPage.error().should('contain', message);
    }
}