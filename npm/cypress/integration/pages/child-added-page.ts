export default class ChildAddedPage {
    static is_on_page(): void {
        cy.get('h1')
        .should('contain', 'Child Added');
    }

    static doneButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=done]');
    }

    static doneButtonIsVisible(): void {
        ChildAddedPage.doneButton()
        .should('be.visible');
    }

    static clickDoneButton(): void {
        ChildAddedPage.doneButton()
        .click();
    }
}