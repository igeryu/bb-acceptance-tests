import { expect } from 'chai';

import {
    STUBBING,
    SIGNUP_URL
} from '../common/test-config';

import { get_random_string } from '../common/test-helper';

export default class SignUpPage {

    static viewSignUpPage(): void {
        cy.visit(SIGNUP_URL);
    }

    static enter_email(email: string): void {
        SignUpPage.emailInput().type(email);
    }

    static enterEmailValid(): void {
        const parentEmail = get_random_string(5) + '@example.com';
        SignUpPage.emailInput().type(parentEmail);
    }

    static enterPasswords(password: string, confirmation: string): void {
        SignUpPage.passwordInput().type(password);
        SignUpPage.passwordConfirmationInput().type(confirmation);
    }

    static click_Submit(): void {
        if (STUBBING) {
            cy.intercept('POST', 'api/credentials/users', {});
        }
        SignUpPage.submitButton().click();
    }

    static click_Submit_duplicate(): void {
        if (STUBBING) {
            const response = {
                body: {},
                statusCode: 409,
            };
            cy.intercept('POST', 'api/credentials/users', response);
        } else {
            throw new Error('Should not be calling click_Submit_duplicate if not stubbing');
        }

        SignUpPage.submitButton().click();
    }

    static header(): Chainable<JQuery<HTMLElement>> {
        return cy.get('h1');
    }

    static is_on_page(isDisplayed: boolean): void {
        if (isDisplayed) {
            SignUpPage.header()
                .should(($h1): void => {
                    expect($h1).to.have.length(1);
                })
                .should('contain', 'Sign Up')
                .should('not.contain', 'Sign Up Complete');
        } else {
            SignUpPage.header()
                .should('not.contain', 'Sign Up');
        }
    }

    static emailInputIsVisible(): void {
        SignUpPage.emailInput().should('be.visible');
    }

    static passwordInputIsVisible(): void {
        SignUpPage.passwordInput().should('be.visible');
    }

    static passwordConfirmationInputIsVisible(): void {
        SignUpPage.passwordConfirmationInput().should('be.visible');
    }

    static Submit_button_is_visible(): void {
        SignUpPage.submitButton()
            .should('be.visible')
            .should('have.attr', 'type', 'submit');
    }

    static errorMessagesAreNotVisible(): void {
        SignUpPage.error()
            .should('not.exist');
    }

    static signinLinkIsVisible(): void {
        SignUpPage.signInLink()
            .should('be.visible');
    }

    static clickSigninLink(): void {
        SignUpPage.signInLink()
            .click();
    }

    static emailConfirmationPageIsDisplayed(): void {
        SignUpPage.header().should('contain', 'Thank you for signing up');
        cy.get('p').should('contain', 'Please check your email for an account confirmation message');
    }

    static error_messages_are_visible(): void {
        SignUpPage.error().should('be.visible');
    }

    static error_message_contains(message: string): void {
        SignUpPage.error().should('contain', message);
    }

    static submitButton(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=submit]');
    }

    static emailInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=email]');
    }

    static passwordInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=password]');
    }

    static passwordConfirmationInput(): Chainable<JQuery<HTMLElement>> {
        return cy.get('[data-cy=matchingPassword]');
    }

    static signInLink(): Chainable<JQuery<HTMLElement>> {
        return cy.get('a.signin');
    }

    static error(): Chainable<JQuery<HTMLElement>> {
        return cy.get('.error');
    }

}