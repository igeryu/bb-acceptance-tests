import { When } from 'cypress-cucumber-preprocessor/steps';

import AddChorePage from '../../pages/add-chore-page';
import AssignChorePage from '../../pages/assign-chore-page';
import ChildsChoresPage from '../../pages/childs-chores-page';
import DashboardPage from '../../pages/dashboard-page';
import SignInPage from '../../pages/signin-page';
import SignUpPage from '../../pages/signup-page';

When('I attempt to access the {word} page', function (pageName: string): void {
    switch (pageName) {
        case 'Sign-Up':
            SignUpPage.viewSignUpPage();
            break;
        case 'Sign-In':
            SignInPage.viewSignInPage();
            break;
        case 'Dashboard':
            DashboardPage.viewDashboardPage();
            break;
        case 'Childs_Chores':
            ChildsChoresPage.viewChildsChoresPage(1234, 'abcd');
            break;
        case 'Assign_Chore':
            AssignChorePage.view_Assign_Chore_page(1234, 'abcd');
            break;
        case 'Add_Chore':
            AddChorePage.view_Add_Chore_page(1234, 'abcd');
            break;
        default:
            throw new Error('Invalid page name "' + pageName + '"');
    }
});