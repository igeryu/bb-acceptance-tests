Feature: Page Security

  Background: As a User, I should only access pages that I am authorized, so the application UX will be better
    Given I am a Parent

  @stubable
  @stub_only
  Scenario Outline: An unauthorized User <access> able to access the <pageName> page
    Given I have an account
    And I am not signed in
    When I attempt to access the <pageName> page
    Then the <pageName> page <access> displayed

    Examples:
      | pageName      | access |
      | Sign-Up       | is     |
      | Sign-In       | is     |
      | Dashboard     | isn't  |
      | Childs_Chores | isn't  |
      | Assign_Chore  | isn't  |
      | Add_Chore     | isn't  |

  @stubable
  @stub_only
  Scenario Outline: An authorized User <access> able to access the <pageName>
    Given I have an account
    And I have already signed in
    When I attempt to access the <pageName> page
    Then the <pageName> page <access> displayed

    Examples:
      | pageName      | access |
      | Sign-Up       | isn't  |
      | Sign-In       | isn't  |
      | Dashboard     | is     |
      | Childs_Chores | is     |
      | Assign_Chore  | is     |
      | Add_Chore     | is     |