import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

import {CHORES_URL} from './test-config';

import {
    I_have_signed_up_and_I_am_viewing_Sign_Up_screen,
    sign_in,
    sign_out,
    sign_in_via_UI,
} from './session-helper';

import AssignChorePage from '../pages/assign-chore-page';
import ChildsChoresPage from '../pages/childs-chores-page';
import DashboardPage from '../pages/dashboard-page';
import SignInPage from '../pages/signin-page';
import SignUpPage from '../pages/signup-page'
import AddChorePage from '../pages/add-chore-page';

Given('I am a Parent', (): void => {
    // TODO nothing to do, for now
});

Given('I have an account', (): void => {
    I_have_signed_up_and_I_am_viewing_Sign_Up_screen();
});

Given('I am not signed in', (): void => {
    sign_out();
});

Given('I have already signed in', (): void => {
    sign_in();
});

When('I access the app', (): void => {
    cy.visit(CHORES_URL);
});

When('I sign in', (): void => {
    sign_in_via_UI();
});

When('the {word} page {word} displayed', function(pageName: string, status: string): void {
    const isDisplayed = status === 'is';
    switch(pageName) {
        case 'Sign-Up':
            SignUpPage.is_on_page(isDisplayed);
            break;
        case 'Sign-In':
            SignInPage.is_on_page(isDisplayed);
            break;
        case 'Dashboard':
            DashboardPage.is_on_page(isDisplayed);
            break;
        case 'Childs_Chores':
            ChildsChoresPage.is_on_page(isDisplayed);
            break;
        case 'Assign_Chore':
            AssignChorePage.is_on_page(isDisplayed);
            break;
        case 'Add_Chore':
            AddChorePage.is_on_page(isDisplayed);
            break;
        default:
            throw new Error('Invalid page name "' + pageName + '"');
    }
});

Then('an Add Child button is visible', (): void => {
    DashboardPage.addChildButtonIsVisible();
});