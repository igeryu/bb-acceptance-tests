import DashboardPage from '../pages/dashboard-page';
import SignUpPage from '../pages/signup-page';
import SignInPage from '../pages/signin-page';

import {
    CHORES_URL,
    STUBBING,
} from './test-config';

import { get_random_string } from './test-helper';

const password = 'asdf_1234'
const NOT_STUBBING = !STUBBING;

afterEach((): void => {
    cy.wrap(undefined).as('email');
    cy.wrap(true).as('signedOut');
});

function enter_credentials_as_is(): void {
    cy.get('@email').then((email: string): void => {
        SignUpPage.is_on_page(true);
        SignUpPage.enter_email(email);
        SignUpPage.enterPasswords(password, password);
    });
}

export function sign_up(): void {
    const email = get_random_string(5) + '@example.com';
    cy.wrap(email).as('email');

    cy.request({
        method: 'POST',
        url: '/api/credentials/users',
        body: {
            username: email,
            password: password,
        },
    });
}

export function sign_up_via_UI(): void {
    const email = get_random_string(5) + '@example.com';
    cy.wrap(email).as('email');

    SignUpPage.viewSignUpPage();
    enter_credentials_as_is();
    SignUpPage.click_Submit();
}

export function sign_up_stubbed(): void {
    const email = get_random_string(5) + '@example.com';
    cy.wrap(email).as('email');
}

export function I_have_signed_up_and_I_am_viewing_Sign_Up_screen(): void {
    if (NOT_STUBBING) {
        sign_up();
        SignUpPage.viewSignUpPage();
    } else {
        sign_up_stubbed();
    }
}

export function enter_credentials_duplicate(): void {
    SignUpPage.viewSignUpPage();
    enter_credentials_as_is();
    cy.wrap(false).as('signedOut');
}

export function sign_in(): void {
    if (NOT_STUBBING) {
        cy.get('@email').then((email: string): void => {
            if (!email) {
                cy.wrap(get_random_string(5) + '@example.com').as('email');
            }
            cy.request({
                method: 'POST',
                url: '/api/auth/oauth/token',
                form: true,
                auth: {
                    user: 'userClient',
                    pass: 'userClientPassword',
                },
                body: {
                    grant_type: 'password',
                    username: email,
                    password: password,
                },
            }).then((response: StaticResponse): void => {
                // response.body is automatically serialized into JSON
                expect(response.body).to.have.property('access_token');
                cy.wrap(response.body.access_token).as('token');
                sessionStorage.setItem('JWT', response.body.access_token);
                sessionStorage.setItem('currentUser', btoa(JSON.stringify({ role: 'PARENT' })));
                DashboardPage.viewDashboardPage();
            })
        });
    } else {
        sign_in_stubbed();
    }

    cy.wrap(false).as('signedOut');
}

export function sign_in_via_UI(): void {
    cy.get('@email').then((email: string): void => {
        cy.get('@signedOut').then((signedOut: boolean): void => {
            if (email === undefined) {
                sign_up_via_UI();
            }

            if (signedOut) {
                SignInPage.viewSignInPage();
                SignInPage.enter_email(email);
                SignInPage.enterPasswords(password);
                SignInPage.click_Submit();
            }

            cy.wrap(false).as('signedOut');
        });
    });
}

function sign_in_stubbed(): void {
    sessionStorage.setItem('JWT', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c');
    sessionStorage.setItem('currentUser', btoa(JSON.stringify({ role: 'PARENT' })));
    DashboardPage.viewDashboardPage();
}

export function sign_out(): void {
    cy.window().then((win): void => {
        win.sessionStorage.clear();
    });
    cy.visit(CHORES_URL);
    cy.wrap(true).as('signedOut');
}