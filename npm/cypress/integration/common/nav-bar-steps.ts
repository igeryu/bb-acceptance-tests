import {
    When,
    Then,
} from 'cypress-cucumber-preprocessor/steps';

When('I click the back button', (): void => {
    click_the_back_button();
});

Then('a back button is visible', (): void => {
    back_button_is_visible();
    back_button_has_correct_content();
    back_button_is_in_correct_location();
});

Then('a back button is not visible', (): void => {
    back_button_is_not_visible();
});

function back_button_is_not_visible(): void {
    backButton()
        .should('not.exist');
}

function back_button_is_visible(): void {
    backButton()
        .should('be.visible');
}

function back_button_is_in_correct_location(): void {
    cy.get('[data-cy=nav-bar]').children()
        .should('have.length', 2)
        .first()
        .should('have.text', '<');
}

function back_button_has_correct_content(): void {
    backButton()
        .should('have.text', '<');
}

function click_the_back_button(): void {
    backButton().click();
}

function backButton(): Chainable<JQuery<HTMLElement>> {
    return cy.get('[data-cy=nav-bar] * [data-cy=button-back]');
}