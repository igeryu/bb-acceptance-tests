import { Before } from 'cypress-cucumber-preprocessor/steps';

import {
    CHORES_URL,
    STUBBING
} from './test-config';

Before({ tags: 'not @stubable' }, function (): void {
    if (STUBBING) {
        throw new Error(`This step is not intended to be used with stubbing`);
    }
});

Before((): void => {
    cy.server();
    if (STUBBING) {
        const choresAvailableResponse = [];
        cy.wrap(choresAvailableResponse).as('choresAvailableResponse');
        // TODO: Change this to intercept
        // https://github.com/cypress-io/cypress/issues/9302
        // [Cypress 6.0.0] Overriding interceptors doesn't work
        cy.route('GET', 'api/chores', choresAvailableResponse).as('get-available-chores');

        const allChildrenResponse = [];
        cy.wrap(allChildrenResponse).as('allChildrenResponse');
        // TODO: Change this to intercept
        // https://github.com/cypress-io/cypress/issues/9302
        // [Cypress 6.0.0] Overriding interceptors doesn't work
        cy.route('GET', 'api/children', allChildrenResponse).as('get-children');
    } else {
        cy.intercept('GET', 'api/chores').as('get-available-chores');
    }
    cy.visit(CHORES_URL);

    cy.wrap(true).as('expectingCreateChildRequest');
    cy.wrap(true).as('expectingCreateChildSuccess');
    cy.wrap('test@example.com').as('childEmail');
    cy.wrap(false).as('duplicateChild');
    cy.wrap(true).as('expectingCreateChoreRequest');
    cy.wrap(true).as('expectingCreateChoreSuccess');
    cy.wrap(false).as('duplicateChore');
    cy.wrap(false).as('duplicateAccount');
});

afterEach((): void => {
    cy.window().then((win): void => {
        win.sessionStorage.clear();
    });
});