export function get_random_string(length: number): string {
    return Math.random().toString(36).substring(length + 1);
}