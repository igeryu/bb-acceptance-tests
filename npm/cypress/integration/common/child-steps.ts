import { Given } from 'cypress-cucumber-preprocessor/steps';

import { STUBBING } from './test-config';

import { get_random_string } from './test-helper';

import ChildHelper from './child-helper';
import DashboardPage from '../pages/dashboard-page';

Given('I added my Child', (): void => {
    ChildHelper.choose_Child_first_name();
    cy.get('@childEmail').then((childEmail: string): void => {
        if (STUBBING) {
            cy.get('@childFirstName').then((childFirstName: string): void => {
                const childId = Math.trunc(Math.random() * 1000);
                const addedChild = { id: childId, firstName: childFirstName, email: childEmail };
                cy.wrap(childId).as('childId');
                cy.get('@allChildrenResponse').then((allChildrenResponse: StaticResponse[]): void => {
                    allChildrenResponse.push(addedChild);
                });
                cy.intercept('GET', `api/children/${childId}`, addedChild).as('get-child');

                const choresAssignedResponse = [];
                cy.wrap(choresAssignedResponse).as('choresAssignedResponse');
                // TODO: Change this to intercept
                // https://github.com/cypress-io/cypress/issues/9302
                // [Cypress 6.0.0] Overriding interceptors doesn't work
                cy.route('GET', `api/children/${childId}/chores`, choresAssignedResponse).as('get-assigned-chores');
            });
        } else {
            cy.wrap(get_random_string(5) + '@example.com').as('childEmail');
            ChildHelper.add_Child();
        }
        DashboardPage.viewDashboardPage();
    });
});

Given('I added my Child, named {string}', (childFirstName: string): void => {
    ChildHelper.set_Child_first_name(childFirstName);

    cy.get('@childEmail').then((childEmail: string): void => {
        if (STUBBING) {
            const childId = Math.trunc(Math.random() * 1000);
            const addedChild = { id: childId, firstName: childFirstName, email: childEmail };
            cy.wrap(childId).as('childId');
            cy.get('@allChildrenResponse').then((allChildrenResponse: StaticResponse[]): void => {
                allChildrenResponse.push(addedChild);
            });
            cy.intercept('GET', `api/children/${childId}`, addedChild).as('get-child');

            const choresAssignedResponse = [];
            cy.wrap(choresAssignedResponse).as('choresAssignedResponse');
            // TODO: Change this to intercept
            // https://github.com/cypress-io/cypress/issues/9302
            // [Cypress 6.0.0] Overriding interceptors doesn't work
            cy.route('GET', `api/children/${childId}/chores`, choresAssignedResponse).as('get-assigned-chores');
        } else {
            cy.wrap(get_random_string(5) + '@example.com').as('childEmail');
            ChildHelper.add_Child();
        }
        DashboardPage.viewDashboardPage();
    });
});

When('I click the Child Button', (): void => {
    click_the_Child_Button();
});

function click_the_Child_Button(): void {
    cy.get('@childFirstName').then((childFirstName: string): void => {
        DashboardPage.click_Child_button(childFirstName);
    });
}

Then('the Child I added is displayed', (): void => {
    cy.get('@childFirstName').then((childFirstName: string): void => {
        DashboardPage.childIsDisplayed(childFirstName);
    });
});