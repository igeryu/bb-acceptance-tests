
import AddChildPage from '../pages/add-child-page';
import DashboardPage from '../pages/dashboard-page';

import { STUBBING } from './test-config';

import { get_random_string } from './test-helper';
import { sign_in } from './session-helper';

export default class ChildHelper {

    static I_sign_in_and_view_Add_Child_screen(): void {
        sign_in();
        this.I_am_viewing_Add_Child_screen();
    }

    static I_am_viewing_Add_Child_screen(): void {
        this.view_add_child_screen();
        cy.wrap(true).as('expectingCreateChildRequest');
        cy.wrap(true).as('expectingCreateChildSuccess');
    }

    static view_add_child_screen(): void {
        DashboardPage.is_on_page(true);
        DashboardPage.click_Add_Child_button();
    }

    static a_valid_email_has_been_entered(): void {
        this.choose_Child_email();
        cy.get('@childEmail').then((childEmail: string): void => {
            AddChildPage.enter_email(childEmail);
        });
    }

    static a_first_name_has_been_entered(): void {
        this.choose_Child_first_name();
        cy.get('@childFirstName').then((childFirstName: string): void => {
            AddChildPage.enter_first_name(childFirstName);
        });
    }

    static choose_Child_first_name(): void {
        cy.wrap(`Child_${get_random_string(5)}`).as('childFirstName');
    }

    static set_Child_first_name(childFirstName): void {
        cy.wrap(childFirstName).as('childFirstName');
    }

    static choose_Child_email(): void {
        cy.wrap(get_random_string(5) + '@example.com').as('childEmail');
    }

    static I_sign_in_and_enter_my_Childs_info(): void {
        this.I_sign_in_and_view_Add_Child_screen();
        this.a_first_name_has_been_entered();
        this.a_valid_email_has_been_entered();
    }

    static I_click_the_Submit_button(): void {
        cy.get('@duplicateChild').then((duplicateChild: boolean): void => {
            if (duplicateChild && STUBBING) {
                AddChildPage.click_Submit_duplicate();
                cy.wrap(false).as('duplicateChild');
            } else {
                cy.get('@expectingCreateChildRequest').then((expectingCreateChildRequest: boolean): void => {
                    cy.get('@expectingCreateChildSuccess').then((expectingCreateChildSuccess: boolean): void => {
                        AddChildPage.click_Submit(expectingCreateChildRequest, expectingCreateChildSuccess);
                    });
                });
            }
        });
    }

    static add_Child(): void {
        cy.get('@token').then((token: string): void => {
            cy.get('@childFirstName').then((childFirstName: string): void => {
                cy.get('@childEmail').then((childEmail: string): void => {
                    cy.request({
                        method: 'POST',
                        url: '/api/children',
                        auth: {
                            bearer: token,
                        },
                        body: {
                            firstName: childFirstName,
                            email: childEmail,
                        },
                    });
                });
            });
        });
    }
};