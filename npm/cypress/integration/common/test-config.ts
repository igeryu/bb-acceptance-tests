let stubbing = Cypress.env('stubbing');
if (stubbing === undefined) {
    stubbing = false;
}

export const STUBBING = stubbing;
export const CHORES_URL = Cypress.env('CHORES_URL');
export const SIGNIN_URL = CHORES_URL + '/ui/login';
export const SIGNUP_URL = CHORES_URL + '/ui/signup';
export const DASHBOARD_URL = CHORES_URL + '/ui/dashboard';