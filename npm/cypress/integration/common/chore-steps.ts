import { Given } from 'cypress-cucumber-preprocessor/steps';

import { STUBBING } from './test-config';

import AddChorePage from '../pages/add-chore-page';
import ChildsChoresPage from '../pages/childs-chores-page';
import ChoreHelper from './chore-helper';
import DashboardPage from '../pages/dashboard-page';

Given('I have already added my Chore, named {string}', (choreName: string): void => {
    the_chosen_Chore_with_name_has_already_been_created(choreName);
});

Given('I have already added my Chore', (): void => {
    the_chosen_Chore_has_already_been_created();
});

When('I click the Assign Chore button', (): void => {
    click_the_Assign_Chore_Button();
});

function click_the_Assign_Chore_Button(): void {
    ChildsChoresPage.is_on_page(true);
    ChildsChoresPage.click_Assign_Chore_button();
}

function the_chosen_Chore_has_already_been_created(): void {
    ChoreHelper.choose_Chore_name();
    if (STUBBING) {
        AddChorePage.add_Chore_to_stubbed_response();
    } else {
        ChoreHelper.add_Available_Chore()
    }
}

function the_chosen_Chore_with_name_has_already_been_created(choreName: string): void {
    cy.get('@childFirstName').then((childFirstName: string): void => {
        ChoreHelper.set_Chore_name(choreName);
        if (STUBBING) {
            AddChorePage.add_Chore_to_stubbed_response();
            DashboardPage.click_Child_button(childFirstName);
            ChildsChoresPage.click_Assign_Chore_button();
        } else {
            ChoreHelper.add_Available_Chore()
            DashboardPage.click_Child_button(childFirstName);
            ChildsChoresPage.click_Assign_Chore_button();
        }
    });
}