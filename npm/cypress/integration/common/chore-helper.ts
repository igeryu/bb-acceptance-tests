import { sign_in } from './session-helper';
import { get_random_string } from './test-helper';

import { STUBBING } from './test-config';

import AddChorePage from "../pages/add-chore-page";
import DashboardPage from '../pages/dashboard-page';
import ChildsChoresPage from '../pages/childs-chores-page';
import AssignChorePage from '../pages/assign-chore-page';

export default class ChoreHelper {

    static I_sign_in_and_enter_Chore_info(): void {
        ChoreHelper.I_sign_in_and_view_Add_Chore_screen();
        ChoreHelper.a_name_has_been_entered();
    }

    static I_sign_in_and_view_Add_Chore_screen(): void {
        sign_in();
        ChoreHelper.I_am_viewing_Add_Chore_screen();
    }

    static I_am_viewing_Add_Chore_screen(): void {
        ChoreHelper.view_Add_Chore_screen();
        cy.wrap(true).as('expectingCreateChoreRequest');
        cy.wrap(true).as('expectingCreateChoreSuccess');
    }

    static view_Add_Chore_screen(): void {
        cy.get('@childFirstName').then((childFirstName: string): void => {
            DashboardPage.is_on_page(true);
            DashboardPage.click_Child_button(childFirstName);
            ChildsChoresPage.is_on_page(true);
            ChildsChoresPage.click_Assign_Chore_button();
            AssignChorePage.is_on_page(true);
            AssignChorePage.click_Add_Chore_button();
            AddChorePage.is_on_page(true);
        });
    }

    static a_name_has_been_entered(): void {
        AddChorePage.is_on_page(true);
        ChoreHelper.choose_Chore_name();
        cy.get('@choreName').then((choreName: string): void => {
            AddChorePage.enter_name(choreName);
        });
    }

    static choose_Chore_name(): void {
        cy.wrap(`Chore_${get_random_string(5)}`).as('choreName');
    }

    static set_Chore_name(choreName: string): void {
        cy.wrap(choreName).as('choreName');
    }

    static I_click_the_Submit_button(): void {
        cy.get('@expectingCreateChoreRequest').then((expectingCreateChoreRequest: boolean): void => {
            cy.get('@expectingCreateChoreSuccess').then((expectingCreateChoreSuccess: boolean): void => {
                cy.get('@duplicateChore').then((duplicateChore: boolean): void => {
                    if (duplicateChore && STUBBING) {
                        AddChorePage.click_Submit_duplicate();
                        cy.wrap(false).as('duplicateChore');
                    } else {
                        AddChorePage.click_Submit(expectingCreateChoreRequest, expectingCreateChoreSuccess);
                    }
                });
            });
        });
    }

    static add_Available_Chore(): void {
        cy.get('@token').then((token: string): void => {
            cy.get('@choreName').then((choreName: string): void => {
                cy.request({
                    method: 'POST',
                    url: '/api/chores',
                    auth: {
                        bearer: token,
                    },
                    body: {
                        name: choreName,
                    },
                });
            });
        });
    }

    static assign_Chore(): void {
        cy.get('@token').then((token: string): void => {
            cy.get('@choreId').then((choreId: number): void => {
                cy.get('@choreName').then((choreName: string): void => {
                    cy.request({
                        method: 'POST',
                        url: `/api/children/${childId}/chores`,
                        auth: {
                            bearer: token,
                        },
                        body: {
                            id: choreId,
                            name: choreName,
                        },
                    });
                });
            });
        });
    }
}