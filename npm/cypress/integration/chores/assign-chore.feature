Feature: Assigning Chores to a Child

    Background: As a Parent, I can assign a Chore to a specific Child, so that other Children don't see that Chore
        Given I am a Parent
        And I have an account
        And I have already signed in

    @stubable
    @stub_only
    Scenario: Assign Existing Chore to Child
        Given I added my Child, named "TestChild123"
        And I have already added my Chore, named "TestChore123"
        And the Assign_Chore page is displayed
        And a confirmation is not displayed
        When I choose a Chore
        Then a confirmation is displayed
        And the Assign_Chore page isn't displayed
        And the Childs_Chores page isn't displayed
        And the confirmation should say "Would you like to assign the chore, TestChore123, to TestChild123?"
        And an option of "Yes" should be displayed
        And an option of "No" should be displayed

    @stubable
    Scenario: Confirm Assignment of Chore to Child
        Given I added my Child
        And I have already added my Chore
        And I click the Child Button
        And I click the Assign Chore button
        And the Assign_Chore page is displayed
        And I choose a Chore
        And a confirmation is displayed
        When I select "Yes"
        Then the Childs_Chores page is displayed
        And the Chore is assigned

    @stubable
    Scenario: Cancel Assignment of Chore to Child
        Given I added my Child
        And I have already added my Chore
        And I click the Child Button
        And I click the Assign Chore button
        And the Assign_Chore page is displayed
        And I choose a Chore
        And a confirmation is displayed
        When I select "No"
        Then the Childs_Chores page is displayed
        And the Chore is not assigned

    @stubable
    @stub_only
    Scenario: View Assigned Chores
        Given I added my Child
        And I have already added my Chore
        And I have already assigned my Chore to my Child
        And I click the Child Button
        When the Childs_Chores page is displayed
        Then the Chore is assigned