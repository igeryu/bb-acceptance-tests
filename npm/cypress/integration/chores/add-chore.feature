Feature: Adding Chores

  Background: As a Parent, I can add a Chore, so that I can assign it to a Child
    Given I am a Parent
    And I have an account
    And I have already signed in

  @stubable
  Scenario: Child's Chores screen
    Given I added my Child
    And the Dashboard page is displayed
    And the Child I added is displayed
    When I click the Child Button
    Then the Childs_Chores page is displayed
    And no Chores are assigned
    And an Assign Chore button is visible
    And a back button is visible

  @stubable
  Scenario: Child's Chores screen, click Back button
    Given I added my Child
    And the Dashboard page is displayed
    And the Child I added is displayed
    And I click the Child Button
    And the Childs_Chores page is displayed
    And an Assign Chore button is visible
    And a back button is visible
    When I click the back button
    Then the Dashboard page is displayed
    And a back button is not visible

  @stubable
  Scenario: Assign Chore screen
    Given I added my Child
    And I click the Child Button
    And the Childs_Chores page is displayed
    When I click the Assign Chore button
    Then the Assign_Chore page is displayed
    And an Add Chore button is visible
    And a back button is visible

  @stubable
  Scenario: Assign Chore screen, click Back button
    Given I added my Child
    And I click the Child Button
    And the Childs_Chores page is displayed
    And I click the Assign Chore button
    And the Assign_Chore page is displayed
    When I click the back button
    Then the Childs_Chores page is displayed

  @stubable
  Scenario: Assign Chore screen, click Back button twice
    Given I added my Child
    And I click the Child Button
    And the Childs_Chores page is displayed
    And I click the Assign Chore button
    And the Assign_Chore page is displayed
    And I click the back button
    And the Childs_Chores page is displayed
    When I click the back button
    Then the Dashboard page is displayed

  @stubable
  Scenario: Add Chore screen
    Given I added my Child
    And I click the Child Button
    And I click the Assign Chore button
    When I click the Add Chore Button
    Then the Add_Chore page is displayed
    And a Chore Name input is displayed
    And a Submit button is displayed
    And a Cancel button is displayed
    And a back button is not visible

  @stubable
  Scenario: Cancel adding a Chore
    Given I added my Child
    And I click the Child Button
    And I click the Assign Chore button
    And I click the Add Chore Button
    And I have entered a Chore Name
    When I click the Cancel button
    Then the Assign_Chore page is displayed

  @stubable
  Scenario: Return to Adding a Chore after Cancel
    Given I added my Child
    And I click the Child Button
    And I click the Assign Chore button
    And I click the Add Chore Button
    And I have entered a Chore Name
    And I click the Cancel button
    When I click the Add Chore Button
    Then the Add_Chore page is displayed
    And a Chore Name input is displayed
    And an error message is not displayed
    And a Submit button is displayed
    And a Cancel button is displayed
    And a back button is not visible

  @stubable
  Scenario: Add a Chore
    Given I added my Child
    And I click the Child Button
    And I click the Assign Chore button
    And I click the Add Chore Button
    And I have entered a Chore Name
    When I click the Submit button
    Then the Assign_Chore page is displayed
    And the added Chore is displayed
    And a back button is visible

  @stubable
  Scenario: View Chores
    Given I added my Child
    And I have already added my Chore
    And I click the Child Button
    And I click the Assign Chore button
    When the Assign_Chore page is displayed
    Then the added Chore is displayed

  @integration_only
  Scenario: Other Chores not Displayed
    Given another Parent with a Chore exists
    And I have an account
    And I have already signed in
    And I added my Child
    And I have already added my Chore
    And I click the Child Button
    And I click the Assign Chore button
    When the Assign_Chore page is displayed
    Then the added Chore is displayed
    And the other Chore is not displayed

  @stubable
  Scenario: Duplicate Chore
    Given I added my Child
    And I have already added my Chore
    And I click the Child Button
    And I click the Assign Chore button
    And I click the Add Chore Button
    And the Add_Chore page is displayed
    And I enter the existing Chore's name
    When I click the Submit button
    Then the Add_Chore page is displayed
    And an error message is displayed
    And the error message indicates "Chore Already Exists"

  @stubable
  @stub_only
  Scenario: Chore not added - failure
    Given I added my Child
    And I click the Child Button
    And I click the Assign Chore button
    And I click the Add Chore Button
    And the Add_Chore page is displayed
    And I have entered a Chore Name
    And the Chore Add will fail
    When I click the Submit button
    Then the Add_Chore page is displayed
    And an error message is displayed
    And the error message indicates "Failure to Add Chore"

  @stubable
  @stub_only
  Scenario: Chore not added - not shown in Assign Chore screen
    Given I added my Child
    And the Chore Add has failed
    When I am viewing the Assign Chore screen
    Then the new Chore is not displayed

  @stubable
  Scenario: Invalid input - Missing Chore Name
    Given I added my Child
    And I click the Child Button
    And I click the Assign Chore button
    And I click the Add Chore Button
    And the Add_Chore page is displayed
    And I have not entered a Chore Name
    When I click the Submit button
    Then the Add_Chore page is displayed
    And an error message is displayed
    And the error message indicates "Missing Chore Name"
