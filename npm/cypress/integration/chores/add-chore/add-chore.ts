import {
    Given,
    When,
    Then,
} from 'cypress-cucumber-preprocessor/steps';

import { STUBBING } from '../../common/test-config';

import {
    sign_in,
    sign_out,
    I_have_signed_up_and_I_am_viewing_Sign_Up_screen,
} from '../../common/session-helper';
import { get_random_string } from '../../common/test-helper';

import AddChorePage from '../../pages/add-chore-page';
import AssignChorePage from '../../pages/assign-chore-page';
import ChildHelper from '../../common/child-helper';
import ChoreHelper from '../../common/chore-helper';
import ChildsChoresPage from '../../pages/childs-chores-page';

Given("I enter the existing Chore's name", (): void => {
    cy.wrap(true).as('duplicateChore');
    create_Chore_duplicate();
});

Given('I have entered a Chore Name', (): void => {
    a_Chore_Name_has_been_entered();
});

Given('another Parent with a Chore exists', (): void => {
    another_Parent_with_a_Chore_exists();
});

Given('the Chore Add has failed', (): void => {
    the_Chore_Add_has_failed();
});

Given('I have not entered a Chore Name', (): void => {
    AddChorePage.nameInput().clear();
    cy.wrap(false).as('expectingCreateChoreRequest');
    cy.wrap(false).as('expectingCreateChoreSuccess');
});

When('I am viewing the Assign Chore screen', (): void => {
    I_am_viewing_the_Assign_Chore_screen();
});

When('I click the Add Chore Button', (): void => {
    click_the_Add_Chore_Button();
});

When('I click the Cancel button', (): void => {
    click_the_Cancel_Button();
});

When('I click the Submit button', (): void => {
    click_the_Submit_Button();
});

Then('an Assign Chore button is visible', (): void => {
    an_Assign_Chore_button_is_visible();
});

Then('an Add Chore button is visible', (): void => {
    an_Add_Chore_button_is_visible();
});

Then('a Chore Name input is displayed', (): void => {
    a_Chore_Name_input_is_displayed();
});

Then('a Submit button is displayed', (): void => {
    a_Submit_button_is_displayed();
});

Then('a Cancel button is displayed', (): void => {
    a_Cancel_button_is_displayed();
});

Then('an error message is not displayed', (): void => {
    a_error_message_is_not_displayed();
});

Then('the added Chore is displayed', (): void => {
    the_added_Chore_is_displayed();
});

Then('the other Chore is not displayed', (): void => {
    the_other_Chore_is_not_displayed();
});

Then('an error message is displayed', (): void => {
    then_an_error_message_is_displayed();
});

Then('the error message indicates {string}', (error: string): void => {
    const errMsgs = [];

    if (error.includes('Chore Already Exists')) {
        errMsgs.push('Chore Already Exists');
    }

    if (error.includes('Failure to Add Chore')) {
        errMsgs.push('Failed to add chore');
    }

    if (error.includes('Missing Chore Name')) {
        errMsgs.push('Chore Name Missing');
    }

    if (errMsgs.length === 0) {
        throw new Error(`Unexpected error message: "${error}"`)
    }

    errMsgs.forEach((errMsg: string): void => {
        AddChorePage.error_message_contains(errMsg);
    });
});

Then('the Chore Add will fail', (): void => {
    given_the_Chore_add_will_fail();
});

Then('the new Chore is not displayed', (): void => {
    cy.get('@choreName').then((choreName: string): void => {
        AssignChorePage.Chore_is_not_displayed(choreName);
    });
});

Then('no Chores are assigned', (): void => {
    ChildsChoresPage.no_Chores_are_Assigned();
});

function I_am_viewing_the_Assign_Chore_screen(): void {
    AssignChorePage.is_on_page(true);
}

function a_Chore_Name_has_been_entered(): void {
    cy.wrap(get_random_string(5)).as('choreName').then((choreName: string): void => {
        AddChorePage.enter_name(choreName);
    });
}

function another_Parent_with_a_Chore_exists(): void {
    I_have_signed_up_and_I_am_viewing_Sign_Up_screen();

    // Create other Child
    sign_in();
    ChildHelper.choose_Child_first_name();
    ChildHelper.choose_Child_email();
    ChildHelper.add_Child();

    // Create other Chore
    ChoreHelper.choose_Chore_name();
    ChoreHelper.add_Available_Chore();

    cy.get('@choreName').then((choreName: string): void => {
        cy.wrap(choreName).as('previousChoreFirstName');

        sign_out();
    });

}

function an_Assign_Chore_button_is_visible(): void {
    ChildsChoresPage.Assign_Chore_button_is_visible();
}

function click_the_Add_Chore_Button(): void {
    AssignChorePage.click_Add_Chore_button();
}

function click_the_Cancel_Button(): void {
    AddChorePage.click_Cancel_button();
}

function click_the_Submit_Button(): void {
    cy.get('@expectingCreateChoreRequest').then((expectingCreateChoreRequest: boolean): void => {
        cy.get('@duplicateChore').then((duplicateChore: boolean): void => {
            cy.get('@expectingCreateChoreSuccess').then((expectingCreateChoreSuccess: boolean): void => {
                if (duplicateChore && STUBBING) {
                    AddChorePage.click_Submit_duplicate();
                    cy.wrap(false).as('duplicateChore');
                } else {
                    AddChorePage.click_Submit(expectingCreateChoreRequest, expectingCreateChoreSuccess);
                }
            });
        });
    });
}

function an_Add_Chore_button_is_visible(): void {
    AssignChorePage.Add_Chore_button();
}

function a_Chore_Name_input_is_displayed(): void {
    AddChorePage.name_input_is_visible();
}

function a_Submit_button_is_displayed(): void {
    AddChorePage.Submit_button_is_visible();
}

function a_Cancel_button_is_displayed(): void {
    AddChorePage.Cancel_button_is_visible();
}

function a_error_message_is_not_displayed(): void {
    AddChorePage.Cancel_button_is_visible();
}

function the_added_Chore_is_displayed(): void {
    cy.get('@choreName').then((choreName: string): void => {
        AssignChorePage.Chore_is_displayed(choreName);
    });
}

function the_other_Chore_is_not_displayed(): void {
    cy.get('@previousChoreFirstName').then((previousChoreFirstName: string): void => {
        AssignChorePage.Chore_is_not_displayed(previousChoreFirstName);
    });
}

function given_the_Chore_add_will_fail(): void {
    cy.wrap(true).as('expectingCreateChoreRequest');
    cy.wrap(false).as('expectingCreateChoreSuccess');
}

function then_the_Add_Chore_screen_is_visible(): void {
    AddChorePage.is_on_page(true);
}

function the_Chore_Add_has_failed(): void {
    ChoreHelper.I_sign_in_and_enter_Chore_info();
    given_the_Chore_add_will_fail();
    ChoreHelper.I_click_the_Submit_button();
    then_the_Add_Chore_screen_is_visible();
    then_an_error_message_is_displayed();
    then_the_error_message_indicates_Failure_to_Add_Chore();
    when_I_click_the_Cancel_button();
}

function then_an_error_message_is_displayed(): void {
    AddChorePage.error_messages_are_visible();
}

function then_the_error_message_indicates_Failure_to_Add_Chore(): void {
    AddChorePage.error_message_contains('Failed to add chore');
}

function when_I_click_the_Cancel_button(): void {
    AddChorePage.click_Cancel_button();
}

function create_Chore_duplicate(): void {
    cy.get('@choreName').then((choreName: string): void => {
        AddChorePage.enter_name(choreName);
        cy.wrap(true).as('expectingCreateChoreRequest');
        cy.wrap(false).as('expectingCreateChoreSuccess');
    });
}
