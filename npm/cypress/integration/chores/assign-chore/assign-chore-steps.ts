import {
    Given,
    When,
    Then,
} from 'cypress-cucumber-preprocessor/steps';

import { STUBBING } from '../../common/test-config';

import AssignChorePage from '../../pages/assign-chore-page';
import ChildsChoresPage from '../../pages/childs-chores-page';
import ChoreHelper from '../../common/chore-helper';

Given('I choose a Chore', (): void => {
    cy.wait('@get-available-chores');
    // Getting "detached from DOM" errors without this wait
    cy.wait(200);
    cy.get('@choreName').then((choreName: string): void => {
        AssignChorePage.click_Chore_button(choreName);
    });
});

Given('I have already assigned my Chore to my Child', (): void => {
    the_chosen_Chore_has_already_been_assigned();
});

When('I select {string}', (selectedOption: string): void => {
    AssignChorePage.confirmation_select_option(selectedOption);
});

Then('a confirmation is not displayed', (): void => {
    AssignChorePage.confirmation_is_not_displayed();
});

Then('a confirmation is displayed', (): void => {
    AssignChorePage.confirmation_is_displayed();
});

Then('the confirmation should say {string}', (expectedMessage: string): void => {
    AssignChorePage.confirmation_should_say(expectedMessage);
});

Then('an option of {string} should be displayed', (expectedOption: string): void => {
    AssignChorePage.confirmation_should_have_option(expectedOption);
});

Then('the Chore is assigned', (): void => {
    cy.get('@choreName').then((choreName: string): void => {
        ChildsChoresPage.Chore_is_Assigned(choreName);
    });
});

Then('the Chore is not assigned', (): void => {
    cy.get('@choreName').then((choreName: string): void => {
        ChildsChoresPage.Chore_is_not_Assigned(choreName);
    });
});

function the_chosen_Chore_has_already_been_assigned(): void {
    ChoreHelper.choose_Chore_name();
    if (STUBBING) {
        ChildsChoresPage.add_Chore_to_stubbed_response();
    } else {
        ChoreHelper.assign_Chore()
    }
}