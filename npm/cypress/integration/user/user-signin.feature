Feature: Sign Into Parent Account

  Background: As a Parent, I can sign in, so that I can view my dashboard

  @stubable
  @stub_only
  Scenario: Sign In Page

    Given I am not signed in
    And I access the app
    When I click the Sign In link
    Then the Sign-In page is displayed
    And an Email input is displayed
    And a Password input is displayed
    And a Submit button is displayed
    And no error messages are displayed
    And a back button is not visible

  @stubable
  Scenario: Display the Dashboard

    Given I am a Parent
    And I have an account
    And I am not signed in
    When I sign in
    Then the Dashboard page is displayed
    And a back button is not visible

  @stubable
  Scenario: Add Child button
    Given I am a Parent
    And I have an account
    And I have already signed in
    When the Dashboard page is displayed
    Then an Add Child button is visible
    And a back button is not visible