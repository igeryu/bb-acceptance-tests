Feature: Adding Children

  Background: As a Parent, I can add a child, so that they will be included in the chore tracking
    Given I am a Parent
    And I have an account

  @stubable
  Scenario: Add Child Screen
    Given I have already signed in
    And the Dashboard page is displayed
    And an Add Child button is visible
    When I click the Add Child button
    Then the Add Child screen is visible
    And a First Name input is displayed
    And an Email input is displayed
    And a Submit button is displayed
    And a Cancel button is displayed
    And a back button is not visible

  @stubable
  Scenario: Cancel Adding a Child
    Given I have already signed in
    And I am viewing the Add Child Screen
    And I enter a first name for my Child
    And I enter a valid email for my Child
    When I click the Cancel button
    Then the Dashboard page is displayed
    And a back button is not visible

  @stubable
  Scenario: Return to Adding a Child after Cancel
    Given I have already signed in
    And I am viewing the Add Child Screen
    And I enter a first name for my Child
    And I enter a valid email for my Child
    And I click the Cancel button
    When I click the Add Child button
    Then the Add Child screen is visible
    And a First Name input is displayed
    And an error message is not displayed
    And an Email input is displayed
    And a Submit button is displayed
    And a Cancel button is displayed
    And a back button is not visible

  @stubable
  Scenario: Add a Child
    Given I have already signed in
    And I am viewing the Add Child Screen
    And I enter a first name for my Child
    And I enter a valid email for my Child
    When I click the Submit button
    Then the Child Added Confirmation page is displayed
    And a Done button is displayed
    And a back button is not visible

  @stubable
  Scenario: Child Added
    Given I have already signed in
    And I am viewing the Add Child Screen
    And I enter a first name for my Child
    And I enter a valid email for my Child
    And I click the Submit button
    And the Child Added Confirmation page is displayed
    And a Done button is displayed
    When I click the Done button
    Then the Dashboard page is displayed
    And the Child I added is displayed
    And a back button is not visible

  @stubable
  Scenario: View Children
    Given I have already signed in
    And I added my Child
    When the Dashboard page is displayed
    Then the Child I added is displayed

  @integration_only
  Scenario: Other Children not Displayed
    Given another Parent with a Child exists
    And I have an account
    And I have already signed in
    And I added my Child
    When the Dashboard page is displayed
    Then the Child I added is displayed
    And the other Child is not displayed

  @stubable
  Scenario: Duplicate Child
    Given I have already signed in
    And I added my Child
    And I am viewing the Add Child Screen
    And the Child's existing account details have been entered
    When I click the Submit button
    Then the Add Child screen is visible
    And an error message is displayed
    And the error message indicates "Child's email already used"
    And a back button is not visible

  @stubable
  @stub_only
  Scenario: Child Not Added - Failure
    Given I have already signed in
    And I am viewing the Add Child Screen
    And I enter a first name for my Child
    And I enter a valid email for my Child
    And the Child Add will fail
    When I click the Submit button
    Then the Add Child screen is visible
    And an error message is displayed
    And the error message indicates "Failure to add child"
    And a back button is not visible

  @stubable
  @stub_only
  Scenario: Child Not Added - Not Shown in Dashboard
    Given the Child Add has failed
    When I am viewing the Add Child Screen
    Then the new Child is not displayed

  @stubable
  @stub_only
  Scenario Outline: Invalid Input - <error>
    Given I am viewing the Add Child Screen
    And I enter <first_name> for my Child
    And I enter <email> for my Child
    When I click the Submit button
    Then the Add Child screen is visible
    And an error message is displayed
    And the error message indicates "<error>"
    And a back button is not visible

    Examples:
      | first_name    | email            | error                                |
      | no first name | a valid email    | Missing First Name                   |
      | a first name  | no email         | Missing Email                        |
      | a first name  | an invalid email | Invalid Email                        |
      | no first name | an invalid email | Missing First Name and Invalid Email |
      | no first name | no email         | Missing First Name and Missing Email |