import { When, Then } from 'cypress-cucumber-preprocessor/steps';

import SignUpPage from '../../pages/signup-page';
import SignInPage from '../../pages/signin-page';

When('I click the Sign In link', (): void => {
    SignUpPage.clickSigninLink();
});

Then('an Email input is displayed', (): void => {
    SignInPage.emailInputIsVisible();
});

Then('a Password input is displayed', (): void => {
    SignInPage.passwordInputIsVisible();
});

Then('a Submit button is displayed', (): void => {
    SignInPage.Submit_button_is_visible();
});

Then('no error messages are displayed', (): void => {
    SignInPage.errorMessagesAreNotVisible();
});