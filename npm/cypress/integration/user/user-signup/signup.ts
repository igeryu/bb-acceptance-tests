import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

import SignUpPage from '../../pages/signup-page'

import { enter_credentials_duplicate } from '../../common/session-helper';

import { STUBBING } from '../../common/test-config';

afterEach((): void => {
    cy.wrap(false).as('duplicateAccount');
});

Given('I am viewing the Sign Up page', (): void => {
    SignUpPage.is_on_page(true);
});

Given('I enter a valid email', (): void => {
    SignUpPage.enterEmailValid();
});

Given("I entery my account credentials", (): void => {
    cy.wrap(true).as('duplicateAccount');
    enter_credentials_duplicate();
});

Given('I enter the same text into both Password fields', (): void => {
    SignUpPage.enterPasswords('asdf1234', 'asdf1234');
});

Given('the same text has not been entered into both Password fields', (): void => {
    SignUpPage.enterPasswords('asdf1234', '1234___asdf');
});

Given('an invalid Email has been entered', (): void => {
    SignUpPage.enter_email('abc invalid email 123');
});

When('I click the Submit button', (): void => {
    cy.get('@duplicateAccount').then((duplicateAccount: boolean): void => {
        if (duplicateAccount && STUBBING) {
            SignUpPage.click_Submit_duplicate()
        } else {
            SignUpPage.click_Submit();
        }
    });
});

Then('an Email input is displayed', (): void => {
    SignUpPage.emailInputIsVisible();
});

Then('a Password input is displayed', (): void => {
    SignUpPage.passwordInputIsVisible();
});

Then('a Re-type Password input is displayed', (): void => {
    SignUpPage.passwordConfirmationInputIsVisible();
});

Then('a Submit button is displayed', (): void => {
    SignUpPage.Submit_button_is_visible();
});

Then('no error messages are displayed', (): void => {
    SignUpPage.errorMessagesAreNotVisible();
});

Then('the Sign In link is displayed', (): void => {
    SignUpPage.signinLinkIsVisible();
});

Then('the Email Confirmation page is displayed', (): void => {
    SignUpPage.emailConfirmationPageIsDisplayed();
});

Then('an error message is displayed', (): void => {
    SignUpPage.error_messages_are_visible();
});

Then('the error message indicates {string}', (errorMsg: string): void => {
    SignUpPage.error_message_contains(errorMsg);
});