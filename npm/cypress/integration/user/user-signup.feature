Feature: Create Parent Account

  Background: As an unregistered user, I can create my Parent account, so that I can set up my family chore tracking

  @stubable
  @stub_only
  Scenario: Sign Up Page
    Given I am not signed in
    When I access the app
    Then the Sign-Up page is displayed
    And an Email input is displayed
    And a Password input is displayed
    And a Re-type Password input is displayed
    And a Submit button is displayed
    And no error messages are displayed
    And the Sign In link is displayed
    And a back button is not visible

  @stubable
  Scenario: User Signs Up
    Given I am viewing the Sign Up page
    And I enter a valid email
    And I enter the same text into both Password fields
    When I click the Submit button
    Then the Email Confirmation page is displayed
    And a back button is visible

  @stubable
  Scenario: User Signs Up, click Back button
    Given I am viewing the Sign Up page
    And I enter a valid email
    And I enter the same text into both Password fields
    And I click the Submit button
    And the Email Confirmation page is displayed
    When I click the back button
    Then the Sign-In page is displayed
    And a back button is not visible

  @stubable
  @stub_only
  Scenario Outline: <scenario>
    Given I am viewing the Sign Up page
    And <email-step>
    And <password-step>
    When I click the Submit button
    Then the Sign-Up page is displayed
    And an error message is displayed
    And the error message indicates "<scenario>"
    And a back button is not visible

    Examples:
      | scenario          | email-step                                      | password-step                                                |
      | Password Mismatch | I enter a valid email                           | the same text has not been entered into both Password fields |
      | Email Invalid     | I enter the same text into both Password fields | an invalid Email has been entered                            |

  @stubable
  Scenario: Duplicate Credentials
    Given I am a Parent
    And I have an account
    And I am viewing the Sign Up page
    And I entery my account credentials
    When I click the Submit button
    Then the Sign-Up page is displayed
    And an error message is displayed
    And the error message indicates "Account Already Exists"
    And a back button is not visible