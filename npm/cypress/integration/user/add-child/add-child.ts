import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

import { STUBBING } from '../../common/test-config';

import AddChildPage from '../../pages/add-child-page';
import ChildAddedPage from '../../pages/child-added-page';
import DashboardPage from '../../pages/dashboard-page';
import ChildHelper from '../../common/child-helper';

import {
    sign_in,
    sign_out,
    sign_up,
} from '../../common/session-helper';
import { get_random_string } from '../../common/test-helper';

Given('another Parent with a Child exists', (): void => {
    if (STUBBING) {
        throw new Error(`This step is not intended to be used with stubbing`);
    }
    sign_up();
    sign_in();
    ChildHelper.choose_Child_first_name();
    cy.get('@childFirstName').as('previousChildFirstName');
    cy.wrap(get_random_string(5) + '@example.com').as('childEmail');
    ChildHelper.add_Child();

    sign_out();
});

Given('I am viewing the Add Child Screen', (): void => {
    ChildHelper.I_sign_in_and_view_Add_Child_screen();
});

Given('the Child\'s existing account details have been entered', (): void => {
    cy.wrap(true).as('duplicateChild');
    create_child_duplicate();
});

Given("I enter a first name for my Child", (): void => {
    ChildHelper.a_first_name_has_been_entered();
});

Given('I enter no first name for my Child', (): void => {
    AddChildPage.firstNameInput().clear();
    cy.wrap(false).as('expectingCreateChildRequest');
    cy.wrap(false).as('expectingCreateChildSuccess');
});

Given('I enter a valid email for my Child', (): void => {
    ChildHelper.a_valid_email_has_been_entered();
});

Given('I enter an invalid email for my Child', (): void => {
    AddChildPage.enter_email('alfj_invalid_email');
    cy.wrap(false).as('expectingCreateChildRequest');
    cy.wrap(false).as('expectingCreateChildSuccess');
});

Given('I enter no email for my Child', (): void => {
    AddChildPage.emailInput().clear();
    cy.wrap(false).as('expectingCreateChildRequest');
    cy.wrap(false).as('expectingCreateChildSuccess');
});

Given('the Child Add has failed', (): void => {
    ChildHelper.I_sign_in_and_enter_my_Childs_info();
    givenTheChildAddWillFail();
    ChildHelper.I_click_the_Submit_button();
    thenTheAddChildScreenIsVisible();
    thenAnErrorMessageIsDisplayed();
    thenTheErrorMessageIndicatesFailureToAddChild();
    when_I_click_the_Cancel_button();
});

When('I click the Add Child button', (): void => {
    DashboardPage.click_Add_Child_button();
});

When('I click the Submit button', (): void => {
    ChildHelper.I_click_the_Submit_button();
});

When('I click the Cancel button', (): void => {
    when_I_click_the_Cancel_button();
});

Then('the Add Child screen is visible', (): void => {
    thenTheAddChildScreenIsVisible();
});

Then('a First Name input is displayed', (): void => {
    AddChildPage.firstNameInputIsVisible();
});

Then('an Email input is displayed', (): void => {
    AddChildPage.emailInputIsVisible();
});

Then('a Submit button is displayed', (): void => {
    AddChildPage.Submit_button_is_visible();
});

Then('a Cancel button is displayed', (): void => {
    AddChildPage.Cancel_button_is_visible();
});

Then('the Child Added Confirmation page is displayed', (): void => {
    ChildAddedPage.is_on_page();
});

Then('a Done button is displayed', (): void => {
    ChildAddedPage.doneButtonIsVisible();
});

Then('the Child Add will fail', (): void => {
    givenTheChildAddWillFail();
});

Then('I click the Done button', (): void => {
    ChildAddedPage.clickDoneButton();
});

Then('the other Child is not displayed', (): void => {
    cy.get('@previousChildFirstName').then((previousChildFirstName: string): void => {
        DashboardPage.childIsNotDisplayed(previousChildFirstName);
    });
});

Then('the new Child is not displayed', (): void => {
    cy.get('@childFirstName').then((childFirstName: string): void => {
        DashboardPage.childIsNotDisplayed(childFirstName);
    });
});

Then('an error message is not displayed', (): void => {
    AddChildPage.errorMessagesAreNotVisible();
});

Then('an error message is displayed', (): void => {
    thenAnErrorMessageIsDisplayed();
});

Then('the error message indicates {string}', (error: string): void => {
    const errMsgs = [];

    if (error.includes('Missing First Name')) {
        errMsgs.push('First Name Missing');
    }

    if (error.includes('Missing Email')) {
        errMsgs.push('Email Missing');
    }

    if (error.includes('Invalid Email')) {
        errMsgs.push('Email Invalid');
    }

    if (error.includes("Child's email already used")) {
        errMsgs.push("Child's email already used");
    }

    if (error.includes('Failure to add child')) {
        errMsgs.push('Failed to add child');
    }

    if (errMsgs.length === 0) {
        throw new Error(`Unexpected error message: "${error}"`)
    }

    errMsgs.forEach((errMsg: string): void => {
        AddChildPage.error_message_contains(errMsg);
    });
});

function when_I_click_the_Cancel_button(): void {
    AddChildPage.clickCancel();
}

function thenTheErrorMessageIndicatesFailureToAddChild(): void {
    AddChildPage.error_message_contains('Failed to add child');
}

function thenAnErrorMessageIsDisplayed(): void {
    AddChildPage.error_messages_are_visible();
}

function givenTheChildAddWillFail(): void {
    cy.wrap(false).as('expectingCreateChildSuccess');
    cy.wrap(true).as('expectingCreateChildRequest');
}

function thenTheAddChildScreenIsVisible(): void {
    AddChildPage.is_on_page();
}

function create_child_duplicate(): void {
    cy.get('@childFirstName').then((childFirstName: string): void => {
        cy.get('@childEmail').then((childEmail: string): void => {
            DashboardPage.viewDashboardPage();
            ChildHelper.view_add_child_screen();
            AddChildPage.enter_email(childEmail);
            ChildHelper.choose_Child_first_name();
            AddChildPage.enter_first_name(childFirstName);
            cy.wrap(true).as('expectingCreateChildRequest');
            cy.wrap(false).as('expectingCreateChildSuccess');
        });
    });
}
